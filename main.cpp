#include <iostream>
#include <string>

int main() {
    std::cout << "Enter line: ";
    std::string str;
    std::getline(std::cin, str);

    std::cout << std::endl;
    std::cout << "Line: " << str << std::endl;
    std::cout << "Line length: " << str.length() << std::endl;
    std::cout << "First symbol: " << str.front() << std::endl;
    std::cout << "Last symbol: " << str.back() << std::endl;

    return 0;
}
